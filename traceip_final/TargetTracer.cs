﻿using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Text;

namespace TargetTracer
{
    public static class Tracer
    {
        internal static List<IntermediateReply> TraceRoute(string hostNameOrAddress, int maxHops, int maxTries, int timeToReach)
        {
            ConcurrentDictionary<string, int> tries = new();

            var results = new List<IntermediateReply>();

            for (int i = 1; i <= maxHops; i++)
            {
                IntermediateReply result = GetTraceRoute(hostNameOrAddress, i, timeToReach);

                results.Add(result);

                if (tries.TryGetValue(result.IP, out _))
                {
                    tries[result.IP]++;
                }
                else
                {
                    tries[result.IP] = 1;
                }

                if (result.IpReplyStatus == "Success" ||
                    (i == maxHops) ||
                    (tries[result.IP] >= maxTries && result.IpReplyStatus == "TimedOut") ||
                    (result.IpReplyStatus != "Success" && result.IpReplyStatus != "TimedOut" && result.IpReplyStatus != "TtlExpired"))
                {
                    return results;
                }
            }

            return results;
        }

        private static IntermediateReply GetTraceRoute(string hostNameOrAddress, int ttl, int timeout)
        {
            const string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            Ping pinger = new();
            PingOptions pingerOptions = new(ttl, true);

            byte[] buffer = Encoding.ASCII.GetBytes(data);

            try
            {
                var stpw = new Stopwatch();
                stpw.Start();

                PingReply reply = pinger.Send(hostNameOrAddress, timeout, buffer, pingerOptions);

                stpw.Stop();

                if (reply.Status == IPStatus.TimedOut)
                {
                    return new(reply.Address.ToString(), reply.Status.ToString(), -1, "No exceptions");
                }
                
                return new(reply.Address.ToString(), reply.Status.ToString(), stpw.ElapsedMilliseconds, "No exceptions");
            }

            catch (Exception excptn)
            {
                string ex = excptn.Message;

                if(excptn.InnerException != null)
                {
                    ex += "\n" + excptn.InnerException;
                }

                return new(hostNameOrAddress, "Exception", -1, ex); 
            }
        }

        internal static async Task<PacketLossData> 
            CalculatePacketLoss(string ip, int timeout, int pingAttempts)
        {
            int failedPings = 0;
            int latencySum = 0;
            int latencyMax = 0;
            int latencyMin = Int32.MaxValue;
            
            const string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            var replies = new List<Task<PingReply>>();
            
            byte[] buffer = Encoding.ASCII.GetBytes(data);

            for (int i = 0; i < pingAttempts; i++)
            {
                replies.Add(Task.Run(async() =>
                {
                    Ping pinger = new();
                    PingOptions pingerOptions = new(64, true);
                    return await pinger.SendPingAsync(ip, timeout, buffer, pingerOptions);
                }));
            }

            await Task.WhenAll(replies);

            foreach (var reply in replies)
            {
                if (reply.Result.Status != IPStatus.Success)
                {
                    failedPings++;
                }
                else
                {
                    latencySum += (int)reply.Result.RoundtripTime;
                    latencyMin = Math.Min(latencyMin, (int)reply.Result.RoundtripTime);
                    latencyMax = Math.Max(latencyMax, (int)reply.Result.RoundtripTime);
                }
            }

            if (failedPings == pingAttempts)
            {
                return new(100, -1, -1, -1);
            }

            var packetLoss = Convert.ToInt32(Convert.ToDouble(failedPings) / Convert.ToDouble(pingAttempts) * 100.0);
            var averagePing = latencySum / (pingAttempts - failedPings);

            return new(packetLoss, averagePing, latencyMin, latencyMax);
        }

        //!!!console output!!!
        internal static void ResultsConsole(IntermediateReply result, int hopNum, int tryNum, int maxTries, int hopNumL)
        {
            if(hopNumL != -1)
            {
                Console.WriteLine("Hops {0} to {1}:\nTarget IP/Address: {2}", hopNum, hopNumL, result.IP);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Status: {result.IpReplyStatus} Tries Overall: {tryNum + hopNumL - hopNum - 1} Tries in a row: {hopNumL - hopNum + 1} Roundtrip Time (in ms): {result.ReplyTime}");
                Console.ResetColor();
                return;
            }

            Console.WriteLine("Hop {0}:\nTarget IP/Address: {1}", hopNum, result.IP);

            switch (result.IpReplyStatus)
            {
                case "Success":
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Status: {result.IpReplyStatus}\nRoundtrip Time (in ms): {result.ReplyTime}");
                    Console.ResetColor();
                    Console.WriteLine("________________________________________________________________________\n");
                    return;

                case "Exception":
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine($"Status: {result.IpReplyStatus}\nException: {result.ErrorMessage}");
                    Console.ResetColor();
                    Console.WriteLine("________________________________________________________________________\n");
                    return;

                case "TtlExpired":
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;

                case "TimedOut" when tryNum < maxTries:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;

                default:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Status: {result.IpReplyStatus} Try: {tryNum}");
                    Console.ResetColor();
                    Console.WriteLine("________________________________________________________________________\n");
                    return;
            }

            Console.WriteLine($"Status: {result.IpReplyStatus} Try: {tryNum} Roundtrip Time (in ms): {result.ReplyTime}");
            Console.ResetColor();
        }
    }

    internal class ConnectionInfo
    {
        public readonly IEnumerable<IntermediateReply> TraceReplies;

        public readonly IEnumerable<IntermediateStatus> Status;

        public readonly PacketLossData PacketLoss;

        internal ConnectionInfo(IEnumerable<IntermediateReply> traceReplies, IEnumerable<IntermediateStatus> status, PacketLossData packetLoss)
        {
            this.TraceReplies = traceReplies;
            this.Status = status;
            this.PacketLoss = packetLoss;
        }
    }

    internal enum IntermediateStatus
    {
        Success,
        Warning,
        Critical
    }
    
    internal class IntermediateReply
    {
        internal readonly string IP;
        internal readonly string IpReplyStatus;
        internal readonly long ReplyTime;
        internal readonly string ErrorMessage;

        internal IntermediateReply(string ip, string ipReplyStatus, long replyTime, string errorMessage)
        {
            this.IP = ip;
            this.IpReplyStatus = ipReplyStatus;
            this.ReplyTime = replyTime;
            this.ErrorMessage = errorMessage;
        }
    }

    internal class PacketLossData
    {
        internal readonly int PacketLoss;
        internal readonly double AveragePing;
        internal readonly int LatencyMin;
        internal readonly int LatencyMax;

        internal PacketLossData(int packetLoss, double averagePing, int latencyMin,int latencyMax)
        {
            this.PacketLoss = packetLoss;
            this.AveragePing = averagePing;
            this.LatencyMin = latencyMin;
            this.LatencyMax = latencyMax;
        }
    }
}