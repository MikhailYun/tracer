﻿using TargetTracer;

const int maxHops = 40; //Maximum number of hops to search for a Target IP, Max TTL
const int maxTries = 15; //Maximum number of tries to get to a Target IP
const int timeToReach = 500; //Maximum number of ms for a reply
const int pingAttempts = 100; //Num of packets to send 

string path = Path.Combine(Directory.GetCurrentDirectory(), "ips.txt"); //temp path for debug
var ipList = new List<string>(); //list of all addresses to trace

using (StreamReader reader = new(path)) //read addresses
{
    while (await reader.ReadLineAsync() is { } targetAddress)
    {
        targetAddress = targetAddress.Trim();

        ipList.Add(targetAddress);
    }
}

var tries = new ConcurrentDictionary<string, int>(); //num of tries per address

var tracingTasks = ipList.Select(ip => 
    Task.Run(() => 
    Tracer.TraceRoute(ip, maxHops, maxTries, timeToReach))).ToList(); //info about trace for each address

var packetLossTasks = ipList.Select(ip => 
    Task.Run(() => 
    Tracer.CalculatePacketLoss(ip, timeToReach, pingAttempts))).ToList(); //info about packet loss

await Task.WhenAll(tracingTasks);
await Task.WhenAll(packetLossTasks);

var ConnectionResults = new List<ConnectionInfo>();

for (int i = 0; i < tracingTasks.Count; i++)
{
    var statusResult = new List<IntermediateStatus>();
    
    foreach (var currReply in tracingTasks[i].Result)
    {
        var currPacketLoss = packetLossTasks[i].Result;
        
        if (currReply.IpReplyStatus == "Success" && currPacketLoss.PacketLoss < 60)
        {
            statusResult.Add(IntermediateStatus.Success);
        }
        else if (currPacketLoss.PacketLoss >= 60)
        {
            statusResult.Add(IntermediateStatus.Warning);
        }
        else
        {
            statusResult.Add(IntermediateStatus.Critical);
        }
    }
    
    ConnectionResults.Add(new(tracingTasks[i].Result, statusResult, packetLossTasks[i].Result));
}



//latency output in console
for(int i = 0; i < packetLossTasks.Count; i++)
{
    var curr = packetLossTasks[i].Result;
    Console.WriteLine($"packet loss to {ipList[i]} is {curr.PacketLoss}%\n" +
                      $"min - {curr.LatencyMin}, avg- {curr.AveragePing}, max - {curr.LatencyMax}\n");
}

//trace details output in console
//yes, this was written by a junior, how could you tell?
for(int i = 0; i < tracingTasks.Count; i++)
{
    var timeoutF = 0;

    Console.WriteLine($"Tracing to: {ipList[i]}");
    
    for (int j = 0; j < tracingTasks[i].Result.Count; j++)
    {
        var currIpData = tracingTasks[i].Result[j];
        if (tries.TryGetValue(currIpData.IP, out _))
        {
            tries[currIpData.IP]++;
        }
        else
        {
            tries[currIpData.IP] = 1;
        }

        if (j != 0 && j != (tracingTasks[i].Result.Count - 1) && 
            (currIpData.IpReplyStatus == "TimedOut") && (tracingTasks[i].Result[j - 1].IpReplyStatus == "TimedOut"))
        {
            while (tracingTasks[i].Result[j].IpReplyStatus == "TimedOut" && j != (tracingTasks[i].Result.Count - 1))
            {
                j++;
            }
            Tracer.ResultsConsole(currIpData, timeoutF, tries[currIpData.IP], maxTries, j);
            tries[currIpData.IP] += (j - timeoutF - 1);
            j--;
        }

        else 
        {
            if (currIpData.IpReplyStatus == "TimedOut" && j != (tracingTasks[i].Result.Count - 1)) timeoutF = j + 1;

            if (j != (tracingTasks[i].Result.Count - 1) && (currIpData.IpReplyStatus == "TimedOut") && (tracingTasks[i].Result[j + 1].IpReplyStatus == "TimedOut")) continue;

            Tracer.ResultsConsole(currIpData, j + 1, tries[currIpData.IP], maxTries, -1);
        }
    }
    tries.Clear();
}